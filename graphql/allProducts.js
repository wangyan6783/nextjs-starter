import gql from 'graphql-tag';

export const ALL_PRODUCTS = gql`
  query allProducts {
    products(first: 5) {
      edges {
        node {
          id
          name
          description
          images {
            url
          }
        }
      }
    }
  }
`;
