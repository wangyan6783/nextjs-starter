import React from 'react';
import Navbar from './Navbar';

const MainLayout: React.FC = (props) => {
  return (
    <>
      <h1>Header</h1>
      <Navbar />
      {props.children}
      <h1>Footer</h1>
    </>
  );
};

export default MainLayout;
