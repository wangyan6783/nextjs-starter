import React from 'react';
import Link from 'next/link';

const Navbar: React.FC = () => {
  return (
    <nav>
      <ul>
        <li>
          <Link href='/'>
            <a>Home - Client Routing</a>
          </Link>
        </li>
        <li>
          <Link href='/products'>
            <a>Saleor Products - Client Routing</a>
          </Link>
        </li>
        <li>
          <Link href='/contact'>
            <a>Contact - Client Routing</a>
          </Link>
        </li>
        <li>
          <Link href='/us'>
            <a>Home - Server Routing</a>
          </Link>
        </li>

        <li>
          <Link href='/us/products'>
            <a>Devacurl Products - Server Routing</a>
          </Link>
        </li>
      </ul>
    </nav>
  );
};

export default Navbar;
