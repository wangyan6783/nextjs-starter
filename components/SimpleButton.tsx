import * as React from 'react';

type Props = {
  text: string;
};

const ButtonYan: React.FC<Props> = ({ text }) => {
  return <button>{text}</button>;
};

export default ButtonYan;
