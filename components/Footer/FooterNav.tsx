import * as React from 'react';
import { withApollo } from '../../libs/apollo';
import { useQuery } from '@apollo/react-hooks';
import { SECONDARY_MENU } from './queries';
import { NavLink } from '../NavLink';

import './scss/index.scss';

const FooterNav: React.FC = () => {
  const { loading, error, data } = useQuery(SECONDARY_MENU);
  if (error) return <h1>Error</h1>;
  if (loading) return <h1>Loading...</h1>;
  console.log(data.shop.navigation.secondary.items);

  return (
    <footer className="footer-nav">
      <div className="container">
        {data.shop.navigation.secondary.items.map((item: any) => (
          <div className="footer-nav__section" key={item.id}>
            <h4 className="footer-nav__section-header">
              <NavLink item={item} />
            </h4>
            <div className="footer-nav__section-content">
              {item.children.map((subItem: any) => (
                <p key={subItem.id}>
                  <NavLink item={subItem} />
                </p>
              ))}
            </div>
          </div>
        ))}
      </div>
    </footer>
  );
};

export default withApollo({ ssr: true })(FooterNav);
