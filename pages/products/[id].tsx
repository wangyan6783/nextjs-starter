import { useRouter } from 'next/router';

const ProductDetailPage: React.FC = () => {
  const router = useRouter();
  const { id } = router.query;
  return <h1>Product Id: {id}</h1>;
};

export default ProductDetailPage;
