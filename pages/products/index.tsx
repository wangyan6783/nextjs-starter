import React from 'react';
import { withApollo } from '../../libs/apollo';
import { useQuery } from '@apollo/react-hooks';
import { ALL_PRODUCTS } from '../../graphql/allProducts';
import Link from 'next/link';

const ProductsPage: React.FC = () => {
  const { loading, error, data } = useQuery(ALL_PRODUCTS);
  if (error) return <h1>Error</h1>;
  if (loading) return <h1>Loading...</h1>;

  console.log(loading, error, data);

  return (
    <>
      <h1>Products</h1>
      <div>
        <ul>
          {data.products.edges.map((product: any) => (
            <li key={product.node.id}>
              <Link href={`/products/[id]`} as={`/products/${product.node.id}`}>
                <a>
                  <p>{product.node.name}</p>
                </a>
              </Link>
              <p>{product.node.description}</p>
            </li>
          ))}
        </ul>
      </div>
    </>
  );
};

export default withApollo({ ssr: true })(ProductsPage);
