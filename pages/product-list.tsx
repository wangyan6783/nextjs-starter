import { GetServerSideProps } from 'next';

const ProductListPage = ({ data }: any) => {
  return <pre>{JSON.stringify(data, null, 2)}</pre>;
};

export const getServerSideProps: GetServerSideProps = async (context: any) => {
  const res = await fetch(
    `https://api-dev.devatech.us/api/v2/pages/find/?site=2&html_path=${context.req.url}`
  );
  const data = await res.json();
  return { props: { data } };
};

export default ProductListPage;
