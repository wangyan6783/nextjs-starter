import React from 'react';
import Button from '../components/Button';
import { FooterNav } from '../components/Footer';

const HomePage: React.FC = () => {
  return (
    <>
      <h1>Home</h1>
      <Button testingContext="testButton">login</Button>
      <Button testingContext="testButton" secondary={true}>
        logout
      </Button>
      <FooterNav />
    </>
  );
};

export default HomePage;
