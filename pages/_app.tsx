import { AppProps } from 'next/app';
import MainLayout from '../components/MainLayout';
import '../styles/app.scss';

const App: React.FC<AppProps> = ({ Component, pageProps }) => {
  return (
    <MainLayout>
      <Component {...pageProps} />
    </MainLayout>
  );
};

export default App;
