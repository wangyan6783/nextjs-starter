import * as React from 'react';
import { storiesOf } from '@storybook/react';
import SimpleButton from '../components/SimpleButton';

storiesOf('Button', module).add('with text', () => {
  return <SimpleButton text="Click Me" />;
});
